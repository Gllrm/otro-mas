import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gllrm-celular',
  templateUrl: './celular.component.html',
  styleUrls: ['./celular.component.css']
})
export class CelularComponent implements OnInit {
  celular = {
    marca: 'HTC',
    modelo: '626 desire',
    color: 'Negro'
  }
  constructor() { }

  ngOnInit() {
  }

}
