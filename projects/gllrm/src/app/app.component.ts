import { Component } from '@angular/core';

@Component({
  selector: 'gllrm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = {
    nombre: 'Gllrm',
    app: 'Project-Gllrm',
    edad: 45
  };
}
